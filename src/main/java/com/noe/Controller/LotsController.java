package com.noe.Controller;

import com.noe.Model.*;
import com.noe.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LotsController {
    @Autowired
    LotsService lotsSer;

    @GetMapping("/lots")
    public List<Lots> index() {
        return lotsSer.findAll();
    }


    @PostMapping("/lots")
    public Lots createLots(@RequestBody Lots body) {

        return lotsSer.save(body);

    }
}
