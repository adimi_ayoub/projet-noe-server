package com.noe.Controller;

import com.noe.Model.*;
import com.noe.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class RoomController {
    @Autowired
    RoomService roomSer;


    @PostMapping("/rooms")
    public Rooms createRoom(@RequestBody Rooms body) {

        return roomSer.save(body);

    }

    @GetMapping("/rooms/{id_room}")
    public Rooms findRooms(@PathVariable String id_room){
        int roomId = Integer.parseInt(id_room);
        return roomSer.findById(roomId);
    }
}
