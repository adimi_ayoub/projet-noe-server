package com.noe.Controller;

import com.noe.Model.*;
import com.noe.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AdressesController {
    @Autowired
    AdresseService adressesSer;

    @PostMapping("/adresses")
    public Adresses createAdresses(@RequestBody Adresses body) {

        return adressesSer.save(body);

    }

    @GetMapping("/adresses/{id_adress}")
    public Adresses findAdress(@PathVariable String id_adress){
        int adressId = Integer.parseInt(id_adress);
        return adressesSer.findById(adressId);
    }


}
