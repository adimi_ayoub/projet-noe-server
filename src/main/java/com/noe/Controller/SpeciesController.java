package com.noe.Controller;


import com.noe.Model.*;
import com.noe.Service.PhylogeneticsService;
import com.noe.Service.SpeciesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SpeciesController {
    @Autowired
    SpeciesService speciesSer;

    @Autowired
    PhylogeneticsService phylogeneticsSer;


    @GetMapping("/species")
    public List<Species> index() {
        return speciesSer.findAll();
    }


    @PostMapping("/species")
    public Species createSpecies(@RequestBody Species body) {

        return speciesSer.save(body);

    }
}

