package com.noe.Controller;


import com.noe.Model.Conditions;
import com.noe.Model.Users;
import com.noe.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class ConditionController {
    @Autowired
    ConditionService conditionSer;



    @PostMapping("/conditions")
    public Conditions createCondition(@RequestBody Conditions body) {

        return conditionSer.save(body);

    }

    @GetMapping("/conditions/{id_condition}")
    public Conditions findCondition(@PathVariable String id_condition){
        int conditionId = Integer.parseInt(id_condition);
        return conditionSer.findById(conditionId);
    }
}
