package com.noe.Controller;

import com.noe.Model.*;
import com.noe.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class UserController {
    @Autowired
    UserService userSer;

    @CrossOrigin
    @GetMapping("/users")
    public List<Users> index() {
        return userSer.findAll();
    }

    @PostMapping("/users")
    public Users createUser(@RequestBody Users body) {

        return userSer.save(body);

    }

    @GetMapping("/users/{id_user}")
    public Users findUser(@PathVariable String id_user){
        int userId = Integer.parseInt(id_user);
        return userSer.findById(userId);
    }

    @PostMapping("/update/user/{id_u}")
    public String update(@RequestBody Users body, @PathVariable String id_u){
        int userId = Integer.parseInt(id_u);
        userSer.updateUser(body,userId);
        return "User have been succefully updated";
    }


}