package com.noe.Controller;

import com.noe.Model.*;
import com.noe.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class WarehouseController {
    @Autowired
    WarehouseService warehouseSer;

    @GetMapping("/warehouses")
    public List<Warehouses> index() {
        return warehouseSer.findAll();
    }

    @PostMapping("/warehouses")
    public Warehouses createWarehouse(@RequestBody Warehouses body) {

        return warehouseSer.save(body);

    }

    @GetMapping("/warehouses/{id_warehouse}")
    public Warehouses findWarehouse(@PathVariable String id_warehouse){
        int warehouseId = Integer.parseInt(id_warehouse);
        return warehouseSer.findById(warehouseId);
    }
}
