package com.noe.Controller;

import com.noe.Model.*;
import com.noe.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class PersonsController {

    @Autowired
    PersonService personSer;


    @GetMapping("persons")
    public List<Persons> index() {
        return personSer.findAll();
    }

    @PostMapping("/persons")
    public Persons createPerson(@RequestBody Persons body) {

        return personSer.save(body);

    }

    @GetMapping("/persons/{id_person}")
    public Persons findPerson(@PathVariable String id_person){
        int personId = Integer.parseInt(id_person);
        return personSer.findById(personId);
    }
}
