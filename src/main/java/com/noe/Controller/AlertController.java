package com.noe.Controller;

import com.noe.Model.*;
import com.noe.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class AlertController {
    @Autowired
    AlertService alertSer;


    @GetMapping("/alerts")
    public List<Alerte> index() {
        return alertSer.findAll();
    }

    @PostMapping("/alerts")
    public Alerte createAlert(@RequestBody Alerte body) {

        return alertSer.save(body);

    }

    @GetMapping("/alerts/{id_alert}")
    public Alerte findAlert(@PathVariable String id_alert){
        int alertId = Integer.parseInt(id_alert);
        return alertSer.findById(alertId);
    }
}
