package com.noe.Controller;

import com.noe.Model.*;
import com.noe.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class ProjectController {

    @Autowired
    ProjectService projectSer;


    @GetMapping("/projects")
    public List<Project> index() {
        return projectSer.findAll();
    }

    @PostMapping("/projects")
    public Project createProject(@RequestBody Project body) {

        return projectSer.save(body);

    }

    @GetMapping("/projects/{id_project}")
    public Project findProject(@PathVariable String id_project){
        int projectId = Integer.parseInt(id_project);
        return projectSer.findById(projectId);
    }
}
