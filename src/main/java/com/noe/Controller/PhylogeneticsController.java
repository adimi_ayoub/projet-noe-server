package com.noe.Controller;

import com.noe.Model.*;
import com.noe.Service.PhylogeneticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class PhylogeneticsController {
    @Autowired
    PhylogeneticsService phyloSer;


    @PostMapping("/phylos")
    public Phylogenetics createPhylo(@RequestBody Phylogenetics body) {

        return phyloSer.save(body);

    }

}


