package com.noe.Controller;

import com.noe.Model.*;
import com.noe.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class ProjectStateController {
    @Autowired
    ProjectStateService projectStateSer;


    @PostMapping("/projectState")
    public ProjectState createProjectState(@RequestBody ProjectState body) {

        return projectStateSer.save(body);

    }

    @GetMapping("/projectState/{id_ps}")
    public ProjectState findProjectState(@PathVariable String id_ps){
        int psId = Integer.parseInt(id_ps);
        return projectStateSer.findById(psId);
    }
}
