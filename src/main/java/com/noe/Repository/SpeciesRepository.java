package com.noe.Repository;


import com.noe.Model.Species;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SpeciesRepository extends JpaRepository<Species, Integer> {


    List<Species> findAll();

    Species save(Species species);
}
