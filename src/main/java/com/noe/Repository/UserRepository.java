package com.noe.Repository;


import com.noe.Model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;



@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {

    List<Users> findAll();

    Users save(Users user);

    Users findById(int userId);

    Users findByEmail(String email);

    Users findByUsername(String username);


}