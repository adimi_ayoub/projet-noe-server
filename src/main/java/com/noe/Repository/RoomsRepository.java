package com.noe.Repository;

import com.noe.Model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface RoomsRepository extends JpaRepository<Rooms, Integer>{

    Rooms save(Rooms room);

    Rooms findById(int roomId);
}
