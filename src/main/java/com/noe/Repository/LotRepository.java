package com.noe.Repository;

import com.noe.Model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface LotRepository extends JpaRepository<Lots, Integer> {

    List<Lots> findAll();

    Lots save(Lots lot);
}
