package com.noe.Repository;

import com.noe.Model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer>{

    List<Project> findAll();

    Project save(Project project);

    Project findById(int projectId);
}
