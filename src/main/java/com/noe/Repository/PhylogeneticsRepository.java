package com.noe.Repository;

import com.noe.Model.Phylogenetics;
import com.noe.Model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhylogeneticsRepository  extends JpaRepository<Phylogenetics, Integer> {

    Phylogenetics save(Phylogenetics phylogenetic);

}
