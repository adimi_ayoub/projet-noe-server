package com.noe.Repository;

import com.noe.Model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ProjectStateRepository extends JpaRepository<ProjectState, Integer>{
    ProjectState save(ProjectState ps);

    ProjectState findById(int projectStateId);
}

