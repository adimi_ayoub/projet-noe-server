package com.noe.Repository;

import com.noe.Model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface PersonsRepository extends JpaRepository<Persons, Integer>{
    List<Persons> findAll();

    Persons save(Persons persons);

    Persons findById(int personId);
}
