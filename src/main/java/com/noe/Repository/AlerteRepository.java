package com.noe.Repository;

import com.noe.Model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;



@Repository
public interface AlerteRepository extends JpaRepository<Alerte, Integer> {

    List<Alerte> findAll();

    Alerte save(Alerte alerte);

    Alerte findById(int alertId);
}
