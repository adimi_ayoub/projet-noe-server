package com.noe.Repository;


import com.noe.Model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface WarehousesRepository extends JpaRepository<Warehouses, Integer> {

    List<Warehouses> findAll();

    Warehouses save(Warehouses warehouse);

    Warehouses findById(int warehouseId);

}
