package com.noe.Model;


import lombok.*;
import javax.persistence.*;
import java.sql.Date;

import java.util.List;

@Entity
@Table(name="lotsType")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LotsType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idLotsType;

    @Column(name="ressourceType")
    private String ressourceType;
}
