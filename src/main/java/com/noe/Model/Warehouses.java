package com.noe.Model;


import lombok.*;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="warehouses")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Warehouses {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idW;

    @Column(name="nbOfSpots")
    private int nbOfSpots;


}

