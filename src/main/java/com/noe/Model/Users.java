package com.noe.Model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name="users")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users extends JwtRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public int getId() {
        return id;
    }

    @Column(name="username")
    private String username;

    @Column(name="email",unique=true)
    private String email;

    @Column(name="name")
    private String name;

    @Column(name="lastname")
    private String lastname;

    @Column(name="password")
    private String password;

    @Column(name="signature")
    private String signature;

    @Column(name="function")
    private String function;


}

