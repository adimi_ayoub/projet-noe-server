package com.noe.Model;

import lombok.*;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="rooms")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Rooms {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idR;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "condition_id", referencedColumnName = "idCondition")
    private Conditions conditions;
}
