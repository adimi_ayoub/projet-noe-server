package com.noe.Model;

import lombok.*;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="stateproject")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectState {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idSp;

    @Column(name="state")
    private String state;
}
