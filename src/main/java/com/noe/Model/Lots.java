package com.noe.Model;

import lombok.*;
import javax.persistence.*;
import java.sql.Date;

import java.util.List;

@Entity
@Table(name="lots")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Lots {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idS;

    @Column(name="entree")
    private Date entree;

    @Column(name="sortie")
    private Date sortie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lotType_id", referencedColumnName = "idLotsType")
    private LotsType lotType;

}
