package com.noe.Model;


import lombok.*;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="phylogenetics")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Phylogenetics {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPhy;

    @Column(name="name")
    private String name;

    /*@OneToMany(mappedBy = "phylogenetics")
    private List<Species> idS;*/



}
