package com.noe.Model;

import lombok.*;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="persons")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Persons {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPerson;


    @Column(name="email",unique=true)
    private String email;

    @Column(name="name")
    private String name;

    @Column(name="lastname")
    private String lastname;
}
