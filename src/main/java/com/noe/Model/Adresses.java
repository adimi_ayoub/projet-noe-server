package com.noe.Model;

import lombok.*;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Adresses")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Adresses {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idA;

    @Column(name="street")
    private String street;

    @Column(name="postalCode")
    private int postalCode;

    @Column(name="city")
    private String city;

    @Column(name="country")
    private String country;



}
