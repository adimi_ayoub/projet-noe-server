package com.noe.Model;

import lombok.*;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Conditions")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Conditions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idCondition;

    @Column(name="conditionp")
    private String conditionp;
}
