package com.noe.Model;



import lombok.*;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="species")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Species {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idS;

    @Column(name="name")
    private String name;

    @Column(name="weight")
    private int weight;

    @Column(name="height")
    private int height;

    /*@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "phylogenetics_id", referencedColumnName = "idPhy")
    private Phylogenetics idPhy;*/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "phylogenetics_id", referencedColumnName = "idPhy")
    private Phylogenetics phylogenetics;

}
