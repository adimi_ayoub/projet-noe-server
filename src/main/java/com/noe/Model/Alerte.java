package com.noe.Model;


import lombok.*;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Alerte")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Alerte {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idAlerte;

    @Column(name="alertDescr")
    private String alertDes;
}
