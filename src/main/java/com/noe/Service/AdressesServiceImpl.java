package com.noe.Service;

import com.noe.Model.*;
import com.noe.Repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class AdressesServiceImpl implements AdresseService{

    @Autowired
    public AdresseRepository adresse;

    @Override
    public Adresses save(Adresses adresses) {
        return adresse.save(adresses);
    }

    @Override
    public Adresses findById(int id) {
        return adresse.findById(id);
    }
}
