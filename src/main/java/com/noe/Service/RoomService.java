package com.noe.Service;

import com.noe.Model.*;

public interface RoomService {
    Rooms save(Rooms room);

    Rooms findById(int roomId);
}
