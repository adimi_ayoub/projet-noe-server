package com.noe.Service;


import com.noe.Model.*;
import com.noe.Repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProjectStateServiceImpl implements ProjectStateService {

    @Autowired
    public ProjectStateRepository pS;



    @Override
    public ProjectState save(ProjectState projectStates) {
        return pS.save(projectStates);
    }

    @Override
    public ProjectState findById(int id) {
        return pS.findById(id);
    }
}
