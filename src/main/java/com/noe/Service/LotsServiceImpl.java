package com.noe.Service;

import com.noe.Model.*;
import com.noe.Repository.LotRepository;
import com.noe.Repository.PhylogeneticsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class LotsServiceImpl implements LotsService {

    @Autowired
    public LotRepository lot;

    @Override
    public List<Lots> findAll() {
        return lot.findAll();
    }

    @Override
    public Lots save(Lots lots) {
        return lot.save(lots);
    }
}
