package com.noe.Service;


import com.noe.Model.*;
import java.util.List;
public interface WarehouseService {

    List<Warehouses> findAll();

    Warehouses save(Warehouses warehouse);

    Warehouses findById(int warehouseId);
}
