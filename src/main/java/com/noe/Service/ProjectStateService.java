package com.noe.Service;

import com.noe.Model.*;
public interface ProjectStateService {

    ProjectState save(ProjectState ps);

    ProjectState findById(int projectStateId);
}
