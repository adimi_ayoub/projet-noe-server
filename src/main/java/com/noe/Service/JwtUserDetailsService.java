package com.noe.Service;

import com.noe.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;



@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    public UserRepository userRep;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.noe.Model.Users users = userRep.findByEmail(username);

        if (users == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(Integer.toString(users.getId()), users.getPassword(), new ArrayList<>());
    }


    public com.noe.Model.Users save(com.noe.Model.Users user){
        com.noe.Model.Users newUser = new com.noe.Model.Users();
        newUser.setUsername(user.getUsername());

        newUser.setEmail(user.getEmail());

        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));

        return userRep.save(newUser);


    }
}

