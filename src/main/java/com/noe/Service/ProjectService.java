package com.noe.Service;

import com.noe.Model.*;
import java.util.List;

public interface ProjectService {

    List<Project> findAll();

    Project save(Project project);

    Project findById(int projectId);
}
