package com.noe.Service;

import com.noe.Model.*;
import com.noe.Repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class WarehouseServiceImpl implements WarehouseService{

    @Autowired
    public WarehousesRepository warehouse;

    @Override
    public List<Warehouses> findAll() {
        return warehouse.findAll();
    }

    @Override
    public Warehouses save(Warehouses warehouses) {
        return warehouse.save(warehouses);
    }

    @Override
    public Warehouses findById(int idW) {
        return warehouse.findById(idW);
    }

}
