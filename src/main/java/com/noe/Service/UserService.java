package com.noe.Service;



import com.noe.Model.Users;
import java.util.List;


public interface UserService {

    List<Users> findAll();

    Users save(Users user);

    Users findById(int userId);

    void updateUser (Users body,Integer id_user);


}