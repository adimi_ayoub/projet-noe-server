package com.noe.Service;


import com.noe.Model.*;


import java.util.List;
public interface LotsService {

    List<Lots> findAll();

    Lots save(Lots lot);
}
