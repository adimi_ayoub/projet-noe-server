package com.noe.Service;

import com.noe.Model.Phylogenetics;
import com.noe.Model.Users;
import com.noe.Repository.PhylogeneticsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PhylogeneticsServiceImpl implements  PhylogeneticsService{

    @Autowired
    public PhylogeneticsRepository phylo;

    @Override
    public Phylogenetics save(Phylogenetics phylos) {
        return phylo.save(phylos);
    }



}
