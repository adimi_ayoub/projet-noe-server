package com.noe.Service;

import com.noe.Model.Users;
import com.noe.Repository.UserRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import org.springframework.security.crypto.password.PasswordEncoder;
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    public UserRepository user;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public List<Users> findAll() {
        return user.findAll();
    }

    @Override
    public Users save(Users users) {
        return user.save(users);
    }

    @Override
    public Users findById(int id) {
        return user.findById(id);
    }

    @Override
    public void updateUser(Users body,Integer id_user) {
        Users user = findById(id_user);
        user.setUsername(body.getUsername());

        user.setEmail(body.getEmail());

        user.setPassword(bcryptEncoder.encode(body.getPassword()));
        save(user);

    }



}