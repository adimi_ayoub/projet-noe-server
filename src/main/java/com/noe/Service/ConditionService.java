package com.noe.Service;

import com.noe.Model.*;
import java.util.List;

public interface ConditionService {

    Conditions save(Conditions condition);

    Conditions findById(int conditionId);
}
