package com.noe.Service;

import com.noe.Model.*;
import com.noe.Repository.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class AlertServiceImpl implements  AlertService{

    @Autowired
    public AlerteRepository alert;


    @Override
    public List<Alerte> findAll() {
        return alert.findAll();
    }

    @Override
    public Alerte save(Alerte alerts) {
        return alert.save(alerts);
    }

    @Override
    public Alerte findById(int id) {
        return alert.findById(id);
    }
}
