package com.noe.Service;

import com.noe.Model.*;
import java.util.List;
public interface PersonService {

    List<Persons> findAll();

    Persons save(Persons persons);

    Persons findById(int personId);
}
