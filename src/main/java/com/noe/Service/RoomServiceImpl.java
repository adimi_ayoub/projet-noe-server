package com.noe.Service;

import com.noe.Model.*;
import com.noe.Repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class RoomServiceImpl implements RoomService{

    @Autowired
    public RoomsRepository room;



    @Override
    public Rooms save(Rooms rooms) {
        return room.save(rooms);
    }

    @Override
    public Rooms findById(int id) {
        return room.findById(id);
    }
}
