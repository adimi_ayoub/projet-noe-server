package com.noe.Service;

import com.noe.Model.Species;

import com.noe.Repository.PhylogeneticsRepository;
import com.noe.Repository.SpeciesRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class SpeciesServiceImpl implements SpeciesService{

    @Autowired
    public SpeciesRepository specie;

    @Autowired
    PhylogeneticsRepository phylo ;

    @Override
    public List<Species> findAll() {
        return specie.findAll();
    }

    @Override
    public Species save(Species species) {
        return specie.save(species);
    }
}
