package com.noe.Service;

import com.noe.Model.*;
import com.noe.Repository.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService{

    @Autowired
    public PersonsRepository person;


    @Override
    public List<Persons> findAll() {
        return person.findAll();
    }

    @Override
    public Persons save(Persons persons) {
        return person.save(persons);
    }

    @Override
    public Persons findById(int id) {
        return person.findById(id);
    }
}
