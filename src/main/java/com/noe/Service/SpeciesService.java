package com.noe.Service;


import com.noe.Model.Species;
import com.noe.Model.Users;

import java.util.List;

public interface SpeciesService {


    List<Species> findAll();

    Species save(Species species);
}
