package com.noe.Service;

import com.noe.Model.*;
import com.noe.Repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ConditionServiceImpl implements ConditionService{
    @Autowired
    public ConditionRepository condition;

    @Override
    public Conditions save(Conditions conditions) {
        return condition.save(conditions);
    }

    @Override
    public Conditions findById(int id) {
        return condition.findById(id);
    }
}
