package com.noe.Service;

import com.noe.Model.*;
import java.util.List;
public interface AlertService {

    List<Alerte> findAll();

    Alerte save(Alerte alerte);

    Alerte findById(int alertId);
}
