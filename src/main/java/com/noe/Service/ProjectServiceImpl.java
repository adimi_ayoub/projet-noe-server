package com.noe.Service;

import com.noe.Model.*;
import com.noe.Repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService{

    @Autowired
    public ProjectRepository project;


    @Override
    public List<Project> findAll() {
        return project.findAll();
    }

    @Override
    public Project save(Project projects) {
        return project.save(projects);
    }

    @Override
    public Project findById(int id) {
        return project.findById(id);
    }
}
